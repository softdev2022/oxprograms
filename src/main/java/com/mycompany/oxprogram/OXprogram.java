/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class OXprogram {
    public static char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    public static char currentPlayer = 'O';
    public static int row,col;
    public static Scanner kb = new Scanner(System.in);
    public static boolean finish = false;
    public static int count = 0;
    
    public static void main(String[] args){
        showWelcome();
        while(true){
            showTable();
            showTurn();
            inputRowColumn();
            process();
            
            if(finish){
                break;
            }
        }
    }
    public static void showTable() {
        for(int r=0; r<table.length;r++){
            for(int c=0;c<table[r].length;c++){
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowColumn() {
        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if(setTable()){
            if(checkWin(table, currentPlayer,col, row)){
                finish = true;
                showWin();
                return;
            }
            if(checkDraw()){
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }  
    }

    public static void showWin() {
        showTable();
        System.out.println(">>>"+currentPlayer+" Win<<<");
    }
    public static void switchPlayer(){
        if(currentPlayer == 'O'){
                currentPlayer = 'X';
        }
        else{
            currentPlayer = 'O';
        }
    }
    
    public static boolean setTable(){
        table[row-1][col-1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char[][] table, char currentPlayer,int col, int row) {
        if(checkVertical(table,currentPlayer,col)){
            return true;
        }
        else if(checkHorizontal(table,currentPlayer,row)){
            return true;
        }
        else if(checkX(table,currentPlayer)){
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table, char currentPlayer,int col) {
       for(int r=0;r<table.length;r++){
           if(table[r][col-1] != currentPlayer){
               return false;
           }
       }
       return true;
    }

    private static boolean checkHorizontal(char[][] table, char currentPlayer,int row) {
        for(int c=0;c<table.length;c++){
           if(table[row-1][c] != currentPlayer){
               return false;
           }
       }
       return true;
    }

    public static boolean checkX(char[][] table, char currentPlayer) {
        if(checkX1(table, currentPlayer)){
            return true;
        }
        else if(checkX2(table, currentPlayer)){
            return true;
        }
        return false;
    }

    private static boolean checkX1(char[][] table, char currentPlayer) {
        for(int i=0;i<table.length;i++){
            if(table[i][i] != currentPlayer){
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table, char currentPlayer) {
        for(int i=0;i<table.length;i++){
            if(table[i][2-i] != currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if(count == 8){
            return  true;
        }
        return  false;
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>>Draw!!!<<<");
    }
}
