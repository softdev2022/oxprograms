/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.oxprogram.OXprogram;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ACER
 */
public class OXProgramTest {
    
    public OXProgramTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char table[][] = {{'O','-','-'},
                                 {'O','-','-'},
                                 {'O','-','-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXprogram.checkVertical(table,currentPlayer,col));
    }
     @Test
    public void testCheckVerticalPlayerOCol2NoWin() {
        char table[][] = {{'-','O','-'},
                                 {'-','O','-'},
                                 {'-','-','-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(false, OXprogram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckPlayerOCol2Win() {
        char table[][] = {{'-','O','-'},
                                 {'-','O','-'},
                                 {'-','O','-'}};
        char currentPlayer = 'O';
        int col = 2;
        int row = 1;
        assertEquals(true, OXprogram.checkWin(table,currentPlayer,col,row));
    }
    @Test
    public void testCheckPlayerOCol3Win() {
        char table[][] = {{'-','-','O'},
                                 {'-','-','O'},
                                 {'-','-','O'}};
        char currentPlayer = 'O';
        int col = 3;
        int row = 1;
        assertEquals(true, OXprogram.checkWin(table,currentPlayer,col,row));
    }
    @Test
    public void testCheckHorizontalPlayerORow1NoWin() {
        char table[][] = {{'O','O','-'},
                                 {'-','-','-'},
                                 {'-','-','-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(false, OXprogram.checkWin(table,currentPlayer,col,row));
    }
    @Test
    public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O','O','O'},
                                 {'-','-','-'},
                                 {'-','-','-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OXprogram.checkWin(table,currentPlayer,col,row));
    }
    @Test
    public void testCheckX1PlayerO1NoWin() {
        char table[][] = {{'O','-','-'},
                                 {'-','O','-'},
                                 {'-','-','-'}};
        char currentPlayer = 'O';
        assertEquals(false, OXprogram.checkX(table,currentPlayer));
    }
    @Test
    public void testCheckX1PlayerO1Win() {
        char table[][] = {{'O','-','-'},
                                 {'-','O','-'},
                                 {'-','-','O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXprogram.checkX(table,currentPlayer));
    }
    @Test
    public void testCheckX2PlayerO1NoWin() {
        char table[][] = {{'-','-','O'},
                                 {'-','O','-'},
                                 {'-','-','-'}};
        char currentPlayer = 'O';
        assertEquals(false, OXprogram.checkX(table,currentPlayer));
    }
    @Test
    public void testCheckX2PlayerO1Win() {
        char table[][] = {{'-','-','O'},
                                 {'-','O','-'},
                                 {'O','-','-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXprogram.checkX(table,currentPlayer));
    }
}
